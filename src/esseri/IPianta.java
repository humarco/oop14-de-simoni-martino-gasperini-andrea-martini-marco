package esseri;

public interface IPianta {

	/**
	 * @return the plants current life
	 */
	double getLife();

	public int getSoliRichiesti();

	TipoTerreno getTerrenoAccettabile();

	void muore();

	void cambia_immagine();

}
